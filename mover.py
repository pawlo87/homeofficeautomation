#! C:\Users\pawel.nowak\AppData\Local\Microsoft\WindowsApps\python.exe

import pyautogui as gui, time
from datetime import datetime

print('Press Ctrl+C to interrupt')
try:
    while True:
        current_time = '{:%H:%M}'.format(datetime.now().time())
        
        email_position_x = 346
        email_position_y = 1050

        email_refresh_x = 62
        email_refresh_y = 124

        terminal_position_x = 270
        terminal_position_y = 1050

        teams_position_x = 438
        teams_position_y = 1059

        gui.moveTo(email_position_x, email_position_y, duration=1)
        gui.click()

        gui.moveTo(email_refresh_x, email_refresh_y, duration=1)
        gui.click()
        print("{}: Email refreshed\n".format(current_time))

        #show log in terminal:
        gui.moveTo(terminal_position_x, terminal_position_y, duration=1)
        gui.click()
        time.sleep(30)

        gui.moveTo(teams_position_x, teams_position_y, duration=1)
        gui.click()

        time.sleep(180)
except KeyboardInterrupt:
    print("Interrupted")